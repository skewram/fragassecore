package fr.fragasse.core;

import org.bukkit.ChatColor;

public abstract class MessageUtility {
    public static String infoMessage(String message) {
        return ChatColor.BLUE + message;
    }
    public static String successMessage(String message) {
        return ChatColor.GREEN + message;
    }
    public static String errorMessage(String message) {
        return ChatColor.RED + message;
    }
}
