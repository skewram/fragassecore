package fr.fragasse.core;

import fr.fragasse.core.Back.BackMain;
import fr.fragasse.core.SilkTouchSpawner.SilkTouchMain;
import fr.fragasse.core.Tpa.TpaMain;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static PluginManager pluginManager;
    private static Main plugin;

    @Override
    public void onEnable() {
        plugin = this;
        pluginManager = getServer().getPluginManager();
        SilkTouchMain.load();
        TpaMain.load();
        BackMain.load();
        System.out.println("Fragasse Core loaded !!!");
    }

    public static PluginManager getPluginManager() {
        return pluginManager;
    }

    public static Main getPlugin() {
        return plugin;
    }

    @Override
    public void onDisable() {
        System.out.println("Fragasse Core unloaded !!!");
    }
}
