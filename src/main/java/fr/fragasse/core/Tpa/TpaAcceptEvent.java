package fr.fragasse.core.Tpa;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TpaAcceptEvent extends Event {
    public static final HandlerList HANDLERS = new HandlerList();
    private Player requester;
    private Player target;
    private Location from;
    private Location to;

    public TpaAcceptEvent(Player requester, Player target, Location from, Location to) {
        this.requester = requester;
        this.target = target;
        this.from = from;
        this.to = to;
    }

    public Location getFrom() {
        return from;
    }

    public Location getTo() {
        return to;
    }

    public Player getRequester() {
        return requester;
    }

    public Player getTarget() {
        return target;
    }

    @Override
    public String getEventName() {
        return super.getEventName();
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
