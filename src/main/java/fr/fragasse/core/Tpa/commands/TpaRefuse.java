package fr.fragasse.core.Tpa.commands;

import fr.fragasse.core.MessageUtility;
import fr.fragasse.core.Tpa.TpaMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.Optional;

public record TpaRefuse(TpaMain pluginBuilder) implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player p))
            return false;

        if (!pluginBuilder.getPlayersRequest().containsValue(p)) {
            p.sendMessage(MessageUtility.errorMessage("Vous n'avez reçu aucune demande de téléportation"));
            return true;
        }

        Optional<Map.Entry<Player, Player>> senderMap = pluginBuilder.getPlayersRequest().entrySet().parallelStream().filter(playerPlayerEntry -> playerPlayerEntry.getValue().equals(p)).findFirst();
        if (senderMap.isPresent()) {
            Player sender = senderMap.get().getKey();
            p.sendMessage(MessageUtility.errorMessage("Demande de téléportation refusé"));
            sender.sendMessage(MessageUtility.errorMessage("Demande de téléportation refusé"));
            pluginBuilder.removePlayerSender(sender);
        }

        pluginBuilder.removePlayerTarget(p);
        return true;
    }
}
