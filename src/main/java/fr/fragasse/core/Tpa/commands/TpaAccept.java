package fr.fragasse.core.Tpa.commands;

import fr.fragasse.core.Main;
import fr.fragasse.core.MessageUtility;
import fr.fragasse.core.Tpa.TpaAcceptEvent;
import fr.fragasse.core.Tpa.TpaMain;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.Optional;

public record TpaAccept(TpaMain pluginBuilder) implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player p))
            return false;

        if (!pluginBuilder.getPlayersRequest().containsValue(p)) {
            p.sendMessage(MessageUtility.errorMessage("Vous n'avez reçu aucune demande de téléportation"));
            return true;
        }

        Optional<Map.Entry<Player, Player>> senderMap = pluginBuilder.getPlayersRequest().entrySet().parallelStream().filter(playerPlayerEntry -> playerPlayerEntry.getValue().equals(p)).findFirst();
        if (senderMap.isPresent()) {
            Player sender = senderMap.get().getKey();
            Main.getPluginManager().callEvent(new TpaAcceptEvent(sender, p, sender.getLocation(), p.getLocation()));
            p.playSound(p, Sound.ENTITY_ARROW_HIT_PLAYER, SoundCategory.AMBIENT, 1f, 1f);
            sender.playSound(p, Sound.ENTITY_ARROW_HIT_PLAYER, SoundCategory.AMBIENT, 1f, 1f);
            p.sendMessage(MessageUtility.successMessage("Demande de téléportation accepté"));
            sender.sendMessage(MessageUtility.successMessage("Demande de téléportation accepté"));
            sender.teleport(p);
            pluginBuilder.removePlayerSender(sender);
        }

        pluginBuilder.removePlayerTarget(p);
        return true;
    }
}
