package fr.fragasse.core.Tpa.commands;

import fr.fragasse.core.Main;
import fr.fragasse.core.MessageUtility;
import fr.fragasse.core.Tpa.TpaMain;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Instrument;
import org.bukkit.Note;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import java.util.Optional;

public record Tpa(TpaMain pluginBuilder) implements CommandExecutor {

    private void sendRequest(Player from, Player target) {
        target.sendMessage(MessageUtility.infoMessage("Vous avez reçu une demande de téléportation de " + ChatColor.GOLD + "" + ChatColor.UNDERLINE + from.getName()));
        target.sendMessage(
                        ChatColor.GREEN + "" + ChatColor.UNDERLINE
                        + "/tpaccept"
                        + ChatColor.RESET + " ou "
                        + ChatColor.GREEN + ChatColor.UNDERLINE
                        + "/tpyes"
                        + ChatColor.RESET + " pour accepter la demande"
        );
        target.sendMessage(
                ChatColor.RED + "" + ChatColor.UNDERLINE
                        + "/tprefuse"
                        + ChatColor.RESET + " ou "
                        + ChatColor.RED + "" + ChatColor.UNDERLINE
                        + "/tpno"
                        + ChatColor.RESET + " pour refuser la demande"
        );
        target.playNote(target.getLocation(), Instrument.BASS_GUITAR, Note.natural(1, Note.Tone.E));
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player p))
            return false;

        //  Check if player has been passed in parameters
        if(strings.length == 0) {
            p.sendMessage(MessageUtility.errorMessage("Vous devez spécifier un joueur !"));
            return true;
        }

        //  Check if player is online
        Optional<? extends Player> targetQuery = Main.getPlugin().getServer().getOnlinePlayers().parallelStream().filter(player -> player.getName().contentEquals(strings[0])).findFirst();
        if (targetQuery.isEmpty()) {
            p.sendMessage(MessageUtility.errorMessage("Le joueur n'est pas connecté au serveur"));
            return true;
        }

        Player target = targetQuery.get();

        if (pluginBuilder.getPlayersRequest().containsKey(p)) {
            p.sendMessage(MessageUtility.errorMessage("Vous avez déjà envoyé une demande de téléportation à un autre joueur"));
            return true;
        } else if (pluginBuilder.getPlayersRequest().containsValue(target)) {
            p.sendMessage(MessageUtility.errorMessage("Ce joueur a déjà une demande de téléportation en attente"));
            return true;
        }

        pluginBuilder.getPlayersRequest().put(p, target);

        BukkitTask task = pluginBuilder.getScheduler().runTaskLaterAsynchronously(Main.getPlugin(), () -> {
            p.sendMessage(MessageUtility.errorMessage("La demande de téléportation a expiré"));
            target.sendMessage(MessageUtility.errorMessage("La demande de téléportation a expiré"));
            pluginBuilder.removePlayerSender(p);
            pluginBuilder.removePlayerTarget(target);
        }, 20L * 30);
        pluginBuilder.getTpaTimeouts().put(p, task);

        sendRequest(p, target);
        p.sendMessage(MessageUtility.successMessage("Votre demande de téléportation a été envoyé"));

        return true;
    }
}
