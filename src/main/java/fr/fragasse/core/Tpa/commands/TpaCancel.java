package fr.fragasse.core.Tpa.commands;

import fr.fragasse.core.MessageUtility;
import fr.fragasse.core.Tpa.TpaMain;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public record TpaCancel(TpaMain pluginBuilder) implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player p))
            return false;

        if (!pluginBuilder.getPlayersRequest().containsKey(p)) {
            p.sendMessage(MessageUtility.errorMessage("Vous n'avez effectué aucune demande de téléportation"));
            return true;
        }

        pluginBuilder.removePlayerSender(p, true);
        return true;
    }
}
