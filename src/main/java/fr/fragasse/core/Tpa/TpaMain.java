package fr.fragasse.core.Tpa;

import fr.fragasse.core.MessageUtility;
import fr.fragasse.core.PluginBuilder;
import fr.fragasse.core.Tpa.commands.Tpa;
import fr.fragasse.core.Tpa.commands.TpaAccept;
import fr.fragasse.core.Tpa.commands.TpaCancel;
import fr.fragasse.core.Tpa.commands.TpaRefuse;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class TpaMain extends PluginBuilder {
    private static TpaMain instance;
    private final HashMap<Player, Player> playersRequest;
    private final HashMap<Player, BukkitTask> tpaTimeouts;
    private final BukkitScheduler scheduler;

    private TpaMain() {
        super("Tpa");
        this.playersRequest = new HashMap<>();
        this.tpaTimeouts = new HashMap<>();
        this.scheduler = Bukkit.getScheduler();

        this.connectListener(new EventListener(this));
        this.addCommand("tpa", new Tpa(this));
        this.addCommand("tpcancel", new TpaCancel(this));
        this.addCommand("tpaccept", new TpaAccept(this));
        this.addCommand("tprefuse", new TpaRefuse(this));
    }

    public HashMap<Player, Player> getPlayersRequest() {
        return playersRequest;
    }

    public HashMap<Player, BukkitTask> getTpaTimeouts() {
        return tpaTimeouts;
    }

    public BukkitScheduler getScheduler() {
        return scheduler;
    }

    public void sendCancelRequest(Player p) {
        p.sendMessage(MessageUtility.errorMessage("La demande de téléportation a été annulé"));
    }
    public void sendCancelRequest(Player p, boolean isTarget) {
        p.sendMessage(MessageUtility.errorMessage("La demande de téléportation a été annulé"));
    }

    public void removePlayerSender(Player p) {
        if (tpaTimeouts.containsKey(p)) {
            tpaTimeouts.get(p).cancel();
            tpaTimeouts.remove(p);
        }
        playersRequest.remove(p);
    }
    public void removePlayerSender(Player p, boolean sendCancel) {
        if(playersRequest.containsKey(p)) {
            if (sendCancel) {
                sendCancelRequest(playersRequest.get(p), true);
                sendCancelRequest(p);
            }
            removePlayerSender(p);
        }
    }

    public void removePlayerTarget(Player p) {
        if(playersRequest.containsValue(p)) {
            Optional<Map.Entry<Player, Player>> senderMap = playersRequest.entrySet().parallelStream().filter(playerPlayerEntry -> playerPlayerEntry.getValue().equals(p)).findFirst();
            if (senderMap.isPresent()) {
                Map.Entry<Player, Player> sender = senderMap.get();
                playersRequest.remove(sender.getKey());
            }
        }
    }
    public void removePlayerTarget(Player p, boolean sendCancel) {
        if(playersRequest.containsValue(p)) {
            Optional<Map.Entry<Player, Player>> senderMap = playersRequest.entrySet().parallelStream().filter(playerPlayerEntry -> playerPlayerEntry.getValue().equals(p)).findFirst();
            if (senderMap.isPresent()) {
                Map.Entry<Player, Player> sender = senderMap.get();
                if (sendCancel) {
                    sendCancelRequest(sender.getKey());
                    sendCancelRequest(sender.getValue(), true);
                }
                removePlayerTarget(sender.getKey());
            }
        }
    }

    public static void load() {
        if (instance == null) {
            instance = new TpaMain();
        }
    }
}