package fr.fragasse.core.Tpa;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public record EventListener(TpaMain pluginBuilder) implements Listener {
    @EventHandler
    private void onPlayerQuit(PlayerQuitEvent e) {
        pluginBuilder.removePlayerSender(e.getPlayer(), true);
        pluginBuilder.removePlayerTarget(e.getPlayer(), true);
    }
}
