package fr.fragasse.core.Back;

import fr.fragasse.core.Back.commands.Back;
import fr.fragasse.core.PluginBuilder;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class BackMain extends PluginBuilder {
    private static BackMain instance;
    private final HashMap<Player, Location> locationHistory;

    private BackMain() {
        super("Back");
        this.locationHistory = new HashMap<>();

        this.connectListener(new EventListener(this));
        this.addCommand("back", new Back(this));
    }

    public HashMap<Player, Location> getLocationHistory() {
        return locationHistory;
    }

    public static void load() {
        if (instance == null) {
            instance = new BackMain();
        }
    }
}