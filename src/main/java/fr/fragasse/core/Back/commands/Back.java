package fr.fragasse.core.Back.commands;

import fr.fragasse.core.Back.BackMain;
import fr.fragasse.core.MessageUtility;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public record Back(BackMain pluginBuilder) implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player p))
            return false;

        if (!pluginBuilder.getLocationHistory().containsKey(p)) {
            p.sendMessage(MessageUtility.errorMessage("Aucune localisation précédente n'a été enregistré"));
            return true;
        }

        p.teleport(pluginBuilder.getLocationHistory().get(p));
        p.sendMessage(MessageUtility.successMessage("Vous avez été téléporté"));
        p.playSound(p, Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
        return true;
    }
}
