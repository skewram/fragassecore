package fr.fragasse.core.Back;

import fr.fragasse.core.MessageUtility;
import fr.fragasse.core.Tpa.TpaAcceptEvent;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.Objects;

public record EventListener(BackMain pluginBuilder) implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    private void onPlayerDie(PlayerDeathEvent e) {
        pluginBuilder.getLocationHistory().put(e.getEntity().getPlayer(), Objects.requireNonNull(e.getEntity().getPlayer()).getLocation());
        e.getEntity().getPlayer().sendMessage(MessageUtility.infoMessage("Vous pouvez retourner à votre dernier point de mort avec la commande " + ChatColor.GOLD + ChatColor.UNDERLINE + "/back"));
    }

    @EventHandler
    private void onTpaAccept(TpaAcceptEvent e) {
        pluginBuilder.getLocationHistory().put(e.getRequester(), e.getFrom());
        e.getRequester().sendMessage(MessageUtility.infoMessage("Vous pouvez retourner à votre dernier point de téléportation avec la commande " + ChatColor.GOLD + ChatColor.UNDERLINE + "/back"));
    }
}
