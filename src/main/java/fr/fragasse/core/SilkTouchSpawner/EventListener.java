package fr.fragasse.core.SilkTouchSpawner;

import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.Objects;

public record EventListener(SilkTouchMain pluginBuilder) implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();

        //  If it's not a spawner abort
        if (!e.getBlock().getBlockData().getMaterial().equals(Material.SPAWNER))
            return;
        //  If it's not a pickaxe abort
        if (!pluginBuilder.getAllowedPickaxes().contains(p.getInventory().getItemInMainHand().getType()))
            return;
        //  If pickaxe is not silk touch abort
        if (!p.getInventory().getItemInMainHand().containsEnchantment(pluginBuilder.getAllowedEnchant()))
            return;

        CreatureSpawner spawner = (CreatureSpawner) e.getBlock().getState();
        e.getBlock().getWorld().dropItemNaturally(e.getBlock().getLocation(), new CustomSpawner(spawner));

        e.getBlock().setType(Material.AIR);
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onBlockPlace(BlockPlaceEvent e) {
        if (!e.getBlock().getType().equals(Material.SPAWNER))
            return;

        CreatureSpawner spawner = (CreatureSpawner) e.getBlockPlaced().getState();
        if (e.getItemInHand().getItemMeta() == null && e.getItemInHand().getItemMeta().getLore() == null && e.getItemInHand().getItemMeta().getLore().size() > 0)
            return;

        if (!Objects.requireNonNull(e.getItemInHand().getItemMeta().getLore()).get(0).startsWith("Spawner à "))
            return;

        spawner.setSpawnedType(
                EntityType.valueOf(
                        Objects.requireNonNull(
                                e.getItemInHand().getItemMeta()).getLore().get(0).replace("Spawner à ", "")
                )
        );
        spawner.update(true);
    }
}
