package fr.fragasse.core.SilkTouchSpawner;

import fr.fragasse.core.Main;
import fr.fragasse.core.PluginBuilder;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.ArrayList;
import java.util.Arrays;

public class SilkTouchMain extends PluginBuilder {
    private static SilkTouchMain instance;
    private final ArrayList<Material> allowedPickaxes = new ArrayList<>(
            Arrays.asList(
                    Material.WOODEN_PICKAXE,
                    Material.STONE_PICKAXE,
                    Material.IRON_PICKAXE,
                    Material.GOLDEN_PICKAXE,
                    Material.DIAMOND_PICKAXE,
                    Material.NETHERITE_PICKAXE
            )
    );

    private final Enchantment allowedEnchant = Enchantment.SILK_TOUCH;

    private SilkTouchMain() {
        super("SilkTouchSpawner");
        this.connectListener(new EventListener(this));
    }

    public ArrayList<Material> getAllowedPickaxes() {
        return allowedPickaxes;
    }

    public Enchantment getAllowedEnchant() {
        return allowedEnchant;
    }

    public static void load() {
        if (instance == null) {
            instance = new SilkTouchMain();
        }
    }
}
