package fr.fragasse.core.SilkTouchSpawner;

import org.bukkit.Material;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class CustomSpawner extends ItemStack {
    public CustomSpawner(CreatureSpawner spawner) {
        super(Material.SPAWNER, 1);
        ItemMeta meta = this.getItemMeta();

        assert meta != null;
        meta.setLore(List.of("Spawner à " + spawner.getSpawnedType().name()));
        this.setItemMeta(meta);
    }
}
