package fr.fragasse.core;

import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;

import java.util.Objects;

public class PluginBuilder {
    private String name;

    protected PluginBuilder(String name) {
        this.name = name;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void connectListener(Listener listener) {
        Main.getPluginManager().registerEvents(listener, Main.getPlugin());
    }

    protected void addCommand(String command, CommandExecutor executor) {
        if (Main.getPlugin().getCommand(command) != null) {
            Objects.requireNonNull(Main.getPlugin().getCommand(command)).setExecutor(executor);
        } else {
            Main.getPlugin().getLogger().warning("Command " + command + " is null");
        }
    }
}
